/*
 * Copyright (C) 2012 Michael L. Schaecher
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define RECOVERY_PROP " echo ""ro.recovery.version=ZenGarden 2.0.3.3-T"" >> /system/build.prop"

void set_recovery_version();

extern int signature_check_enabled;
extern int script_assert_enabled;

void toggle_signature_check();

void toggle_script_asserts();

void show_power_menu();

void show_mount_usb_storage_menu();

void show_partition_menu();

void show_advanced_menu();

void show_mount_usb_storage_menu();

void handle_failure(int ret);

int verify_root_and_recovery();




